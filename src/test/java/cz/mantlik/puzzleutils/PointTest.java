package cz.mantlik.puzzleutils;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PointTest {

	private Point testedPoint;
	private Point otherPoint;

	@Before
	public void setUp() throws Exception {
		testedPoint = new Point(0, 0, 0);
		otherPoint = new Point(5, 20, -1);
	}

	@Test
	public void addTest() {
		testedPoint.add(otherPoint);
		assertThat(testedPoint, is(otherPoint));
	}

	@Test
	public void manhattanDistanceTest() {
		int manhattanDistance = testedPoint.manhattanDistance(otherPoint);
		assertThat(manhattanDistance, is(26));
	}

	@Test
	public void pointInBoundsTest() {
		boolean result = testedPoint.isInBounds(new Point(-1, -1, -1), new Point(1, 1, 1));
		assertThat(result, is(true));
		result = testedPoint.isInBounds(new Point(1, 1, 1), new Point(2, 2, 2));
		assertThat(result, is(false));
	}

	@Test
	public void getNeighborsTest() {
		List<Point> neighbors = testedPoint.getNeighbors();
		List<Point> expectedNeighbors = prepareNeighbors();
		assertThat(neighbors.size(), is(expectedNeighbors.size()));
		assertThat(neighbors, contains(expectedNeighbors.toArray()));
	}

	private List<Point> prepareNeighbors() {
		List<Point> neighbors = new ArrayList<>();
		for (int x = -1; x <=1 ; x++) {
			for (int y = -1; y <=1 ; y++) {
				for (int z = -1; z <=1 ; z++) {
					neighbors.add(new Point(x, y, z));
				}
			}
		}
		neighbors.remove(testedPoint);
		return neighbors;
	}

}