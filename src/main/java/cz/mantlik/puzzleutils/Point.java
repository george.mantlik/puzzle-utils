package cz.mantlik.puzzleutils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class representing a point in n-dimensional Euclidean space.
 */
public class Point {

	public static final int X = 0;
	public static final int Y = 1;
	public static final int Z = 2;

	private final List<Integer> coordinates = new ArrayList<>();

	private Point(Stream<Integer> coordinatesStream) {
		this.coordinates.addAll(coordinatesStream.collect(Collectors.toList()));
	}

	/**
	 * Instantiate new Point with coordinates given as Integers
	 */
	public Point(Integer ... coordinates) {
		this(Stream.of(coordinates));
	}

	/**
	 * Instantiate new Point with coordinates given as Strings
	 */
	public Point(String ... coordinates) {
		this(Stream.of(coordinates).map(Integer::parseInt));
	}

	/**
	 * Instantiate new Point with same coordinates as given Point
	 */
	public Point(Point other) {
		this(other.coordinates.stream());
	}

	/**
	 * Get coordinates for axis identified by given index
	 */
	public int get(int axisIndex) {
		return coordinates.get(axisIndex);
	}

	/**
	 * Set coordinates for axis identified by given index
	 */
	public void set(int axisIndex, int coordinate) {
		coordinates.set(axisIndex, coordinate);
	}

	/**
	 * Add coordinates of given Point to coordinates of this Point
	 */
	public void add(Point other) {
		for (int i = 0; i < coordinates.size(); i++) {
			set(i, get(i) + other.get(i));
		}
	}

	/**
	 * Count manhattan distance from given Point to this Point
	 */
	public int manhattanDistance(Point other) {
		int distance = 0;
		for (int i = 0; i < coordinates.size(); i++) {
			distance += Math.abs(get(i) - other.get(i));
		}
		return distance;
	}

	/**
	 * Check whether this Point is in area bounded by given corners
	 * @param start smallest possible coordinates
	 * @param end largest possible coordinates
	 * @return True if this Point is in area bounded by given corners
	 */
	public boolean isInBounds(Point start, Point end) {
		for (int i = 0; i < coordinates.size(); i++) {
			int coordinate = coordinates.get(i);
			if (coordinate < start.get(i) || coordinate > end.get(i)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Get copy of this point with coordinates reversed
	 */
	public Point getReverse() {
		return new Point(coordinates.stream().map(c -> -c));
	}

	/**
	 * Get all Points that are direct neighbors of this Point
	 */
	public List<Point> getNeighbors() {
		List<Point> neighbors = generateNeighbors(Collections.emptyList());
		neighbors.remove(this);
		return neighbors;
	}

	private List<Point> generateNeighbors(List<Integer> neighborCoordinates) {
		if (neighborCoordinates.size() == coordinates.size()) {
			return Collections.singletonList(new Point(neighborCoordinates.stream()));
		}

		List<Point> neighbors = new ArrayList<>();
		for (int i = -1; i <= 1; i++) {
			List<Integer> newNeighborCoordinates = new ArrayList<>(neighborCoordinates);
			newNeighborCoordinates.add(coordinates.get(neighborCoordinates.size()) + i);
			neighbors.addAll(generateNeighbors(newNeighborCoordinates));
		}
		return neighbors;
	}

	@Override
	public int hashCode() {
		return Objects.hash(coordinates);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Point)) {
			return false;
		}
		Point other = (Point) obj;
		return this.coordinates.equals(other.coordinates);
	}

	@Override
	public String toString() {
		StringJoiner joinedCoordinates = new StringJoiner(", ");
		coordinates.forEach(c -> joinedCoordinates.add(c.toString()));

		return "Point[" + joinedCoordinates.toString() + "]";
	}
}
