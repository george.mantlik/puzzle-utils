package cz.mantlik.puzzleutils;

/**
 * Utils class for some mathematics operations
 */
public class MathUtils {

	private MathUtils() {
	}

	/**
	 * Counts sum of simple arithmetic series (1 to n) or (-1 to -n)
	 * @param n		Last element of the series (may be negative)
	 * @return		Sum of elements in the series
	 */
	public static int simpleArithmeticSeries(int n) {
		return arithmeticSeries(1, Math.abs(n), n);
	}

	/**
	 * Counts sum of arithmetic series
	 * @param first First element of the series
	 * @param last  Last element of the series
	 * @param n		Number of elements in the series
	 * @return		Sum of elements in the series
	 */
	public static int arithmeticSeries(int first, int last, int n) {
		return n * (first + last) / 2;
	}

	/**
	 * Count absolute value of two provided integers and return the smaller one
	 */
	public static int minOfAbs(int a, int b) {
		return Math.min(Math.abs(a), Math.abs(b));
	}

	/**
	 * Count absolute value of two provided integers and return the bigger one
	 */
	public static int maxOfAbs(int a, int b) {
		return Math.max(Math.abs(a), Math.abs(b));
	}

	/**
	 * Count absolute value of two provided longs and return the smaller one
	 */
	public static long minOfAbs(long a, long b) {
		return Math.min(Math.abs(a), Math.abs(b));
	}

	/**
	 * Count absolute value of two provided longs and return the bigger one
	 */
	public static long maxOfAbs(long a, long b) {
		return Math.max(Math.abs(a), Math.abs(b));
	}

	/**
	 * Count least common multiple of two provided longs
	 */
	public static long lcm(long a, long b) {
		if (a == 0 || b == 0) {
			return 0;
		}

		long absHigherNumber = maxOfAbs(a, b);
		long absLowerNumber = minOfAbs(a, b);
		long lcm = absHigherNumber;
		while (lcm % absLowerNumber != 0) {
			lcm += absHigherNumber;
		}
		return lcm;
	}

}
