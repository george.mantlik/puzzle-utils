package cz.mantlik.puzzleutils;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class PuzzleLogger {

	public static final String DRAW_LINE = "█";
	public static final String DRAW_SPACE = " ";

	private final Logger logger;

	private PuzzleLogger(String name) {
		try {
			InputStream stream = PuzzleLogger.class.getClassLoader().getResourceAsStream("logging.properties");
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger = Logger.getLogger(name);
	}

	public static PuzzleLogger getLogger(String name) {
		return new PuzzleLogger(name);
	}

	public void info(String message, Object ... params) {
		log(Level.INFO, message, params);
	}

	public void error(String message, Object ... params) {
		log(Level.SEVERE, message, params);
	}

	public void debug(String message, Object ... params) {
		log(Level.FINE, message, params);
	}

	private void log(Level level, String message, Object... params) {
		logger.log(level, message, params);
	}

}
