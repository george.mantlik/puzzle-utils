package cz.mantlik.puzzleutils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class PuzzleReader {

	private static final PuzzleLogger LOG = PuzzleLogger.getLogger(PuzzleReader.class.getName());

	public void readFile(String fileName, Consumer<String> action) {
		URL fileUrl = getClass().getClassLoader().getResource(fileName);
		try (Stream<String> stream = Files.lines(Paths.get(Objects.requireNonNull(fileUrl).toURI()))) {
			stream.forEach(action);
		} catch (IOException | URISyntaxException e) {
			LOG.error("Error while reading input.");
		}
	}

}
